var controls = document.getElementsByClassName("bet-input__control");

function setAmount(betAmount) {
    let betAmountBig = betAmount * 100;

    let clicksOn100 = Math.floor(betAmountBig / 10000);

    let remainderFor10 = betAmountBig % 10000;
    let clicksOn10 = Math.floor(remainderFor10 / 1000);

    let remainderFor1 = remainderFor10 % 1000;
    let clicksOn1 = Math.floor(remainderFor1 / 100);

    let remainderFor01 = remainderFor1 % 100;
    let clicksOn01 = Math.floor(remainderFor01 / 10);

    let remainderFor001 = remainderFor01 % 10;
    let clicksOn001 = Math.floor(remainderFor001 / 1);

    for(i = 0; i < clicksOn100; i ++) {
        console.log("click100();");
        console.log(clicksOn100);
        click100();
    }
    for(i = 0; i < clicksOn10; i ++) {
        console.log("click10();");
        console.log(clicksOn10);
        click10();
    }
    for(i = 0; i < clicksOn1; i ++) {
        console.log("click1();");
        console.log(clicksOn1);
        click1();
    }
    for(i = 0; i < clicksOn01; i ++) {
        console.log("click01();");
        console.log(clicksOn01);
        click01();
    }
    for(i = 0; i < clicksOn001; i ++) {
        console.log("click001();");
        console.log(clicksOn001);
        click001();
    }    
}

//0.01
function click001() {
    var zeroPointZeroOne = controls[1];
    zeroPointZeroOne.click();
};

//0.1
function click01() {
    var zeroPointZeroOne = controls[2];
    zeroPointZeroOne.click();
};

//1
function click1() {
    var zeroPointZeroOne = controls[3];
    zeroPointZeroOne.click();
};

//10
function click10() {
    var zeroPointZeroOne = controls[4];
    zeroPointZeroOne.click();
};

//100
function click100() {
    var zeroPointZeroOne = controls[5];
    zeroPointZeroOne.click();
};

setAmount(betAmount);