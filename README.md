Step to Scam:
1. **Down this repository**

![Screenshot](images/downloadRepo.jpg)

2. **Extract it**
3. **Open Chrome**
4. **Type in *chrome://extensions***
5. **Turn on Developer mode**

![Screenshot](images/chromeExtensions.jpg)

6. **Click 載入未封裝項目**

![Screenshot](images/installToChrome.jpg)

7. **Find the path of the repository you just downloaded**
8. **Go to csgoempire.com**
9. **It is done if you see a big green area on the top.**