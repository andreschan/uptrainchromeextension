var rouletteSocket = null;
var baseBetAmount = 0;
var maxBetRounds = 0;
var betRecord = "";
var betCount = 0;
var rollRecord = "";
var roundsToDoubleBet = 0;

window.onload = function () {
    let setAmount = document.getElementById("setAmount");
	let baseBet = document.getElementById('baseBet');
	let roundsToDouble = document.getElementById('roundsToDouble');
	let maxRounds = document.getElementById('maxRounds');
	let stopSocket = document.getElementById('stopSocket');
	let startSocket = document.getElementById('startSocket');
	let recordMsg = document.getElementById('recordMsg');
	let betCountMsg = document.getElementById('betCountMsg');
	let socketMsg = document.getElementById('socketMsg');

	setAmount.onclick = function(element) {
		baseBetAmount = parseFloat(baseBet.value);
		chrome.tabs.executeScript({
			code: 'var betAmount = ' + JSON.stringify(baseBetAmount)
		}, function() {
			chrome.tabs.executeScript({
		 	 	file: 'set_amount.js'
			});
		});
	};

	
	startSocket.onclick = function(element) {
		if (rouletteSocket == null) {
			setUpSocket();
		}
	};

	stopSocket.onclick = function(element) {
		if (rouletteSocket != null) {
			rouletteSocket.close();	
			rouletteSocket = null;
			socketMsg.value = "manually rouletteSocket.close();";
		}
	};
};


// setAmount.onclick = function(element) {
// 	baseBetAmount = baseBet.value;
// 	// chrome.tabs.executeScript({
// 	// 	code: 'var betAmount = ' + JSON.stringify(baseBetAmount)
// 	// }, function() {
// 	// 	chrome.tabs.executeScript({
// 	//  	 	file: 'set_amount.js'
// 	// 	});
// 	// });
// };

// clickDice.onclick = function(element) {
// 	// let _baseBet = baseBet.value;	
// 	// chrome.tabs.executeScript({
// 	// 	code: 'var betAmount = ' + JSON.stringify(_baseBet)
// 	// }, function() {
// 	// 	chrome.tabs.executeScript({
// 	//  	 	file: 'set_amount.js'
// 	// 	});
// 	// });
// 	chrome.tabs.executeScript({
// 		file: "bet_dice.js"
// 	});
// };

//+'; var gapRounds = ' + JSON.stringify(_gapRounds)

function setUpSocket() {
	console.log('setUpSocket');
	rouletteSocket = new WebSocket("wss://roulette.csgoempire.com/s/?EIO=3&transport=websocket");
	roundsToDoubleBet = parseInt(roundsToDouble.value, 10);
	maxBetRounds = parseInt(maxRounds.value, 10);

	rouletteSocket.onopen = function(event) {
		betCountMsg.value = 0;
		rouletteSocket.send("40/roulette,");
	};

	rouletteSocket.onmessage = function(event) {
		
		var msg = event.data;
		socketMsg.value = msg;

		if (msg.includes("rolling")) {

			if (roundsToDoubleBet != 0 && betCount != 0 && betCount%roundsToDoubleBet == 0) {
				chrome.tabs.executeScript({
					file: "double_amount.js"
				});
				baseBetAmount += parseFloat(baseBetAmount);
			}

			rouletteSocket.send("2");
			wait(1000);
			chrome.tabs.executeScript({
				file: "bet_dice.js"
			});

			betRecord += baseBetAmount + ",";
			betCount += 1;
			betCountMsg.value = betCount;
			recordMsg.value = betRecord;
		};
		
		if (msg.includes("\"end\"")) {
			if (maxBetRounds == 0) {
				socketMsg.value = escape("繼續上全力上");	
			}
			socketMsg.value = escape("仲有:") + (maxBetRounds - betCount);
			if (maxBetRounds != 0 && betCount == maxBetRounds) {					
				rouletteSocket.close();
				rouletteSocket= null;
				socketMsg.value = "auto rouletteSocket.close();";
			}
		};
	}

}

function wait(ms){
	var start = new Date().getTime();
	var end = start;
	while(end < start + ms) {
	  end = new Date().getTime();
   }
 }